var express = require('express');
var router = express.Router();
var httpRequest = require('../utils/httpRequest');

const availableRoutes = {
  '/stock': {
    method: 'GET',
    params: 'symbols',
    description: 'Retrieves info for one or more symbols from worldtradingdata API'
  },
  '/watchlist': {
    method: 'GET',
    description: 'Retrieves watchlist with info'
  },
  '/watchlist/insert': {
    method: 'POST',
    params: 'quote',
    description: 'Insert new quote to watchlist'
  },
  '/watchlist/remove': {
    method: 'DELETE',
    params: 'quotes',
    description: 'Removes quote from watchlist'
  }
}

/* GET home page. */
router.get('/', (req, res, next) => {
  res.send(availableRoutes);
});

/* GET symbol(s) data */
router.get('/stock', (req, res, next) => {
  httpRequest.request(req.query.symbols, (results) => {
    const httpStatusCode = results.error ? 500 : 200;
    res.status(httpStatusCode);
    res.send(results);
  });
});

/* GET watchlist */
router.get('/watchlist', (req, res, next) => {
  //user account identification passed
  //will not be used since no authentication
  //will be implemented
  const accountId = req.accountId;
  const watchlist = process.env.watchlist.length > 0 
    ? process.env.watchlist 
    : process.env.defaultWatchlist;
  
  process.env.watchlist = watchlist;
    
  httpRequest.request(watchlist, (results) => {
    const httpStatusCode = results.error ? 500 : 200;
    res.status(httpStatusCode);
    res.send(results);
  });
});

/* POST add quote to watchlist */
router.post('/watchlist/insert', (req, res, next) => {
  const newQuote = req.body.quote;
  const watchListArray = process.env.watchlist.split(',');

  if (watchListArray.indexOf(newQuote) >=0 ) {
    //quote already exists in watchlist
    res.status(400);
    res.send({ error: 'Quote already exists in watchlist!' })
  } else {
    //add quote to watchlist
    httpRequest.request(newQuote, (results) => {
      if(results.error) {
        res.status(500);
      } else {
        watchListArray.push(newQuote);
        process.env.watchlist = watchListArray;
      }

      res.send(results);
    });
  }
});

/* DEL remove quote from watchlist */
router.delete('/watchlist/remove', (req, res, next) => {
  const quotes = req.body.quotes;
  const watchListArray = process.env.watchlist.split(',');
  
  quotes.forEach(quote => {
    if (watchListArray.indexOf(quote) >= 0 ) {
      //quote is in the watchlist
      watchListArray.splice(watchListArray.indexOf(quote), 1);
    } else {
      //quote is not in the watchlist
      res.status(400);
      res.send({ error: 'Quote(s) not found in watchlsit' });
    }
  });

  process.env.watchlist = watchListArray;
  res.send('OK')
});

module.exports = router;
