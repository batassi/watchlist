const https = require('https');

const token = 'mFnPrfmVoRyqbkHjJf4KlRGyNV5CKpQuZDRV4v3UT7DcdBIcGP6rvGudZLhO';
const defaultOptions = {
    host: 'api.worldtradingdata.com',
    post: 443,
    path: '/api/v1/stock?symbol=<symbols>&api_token=' + token,
    method: 'GET'
};

exports.request = (symbols, callback) => {
    const options = {
        ...defaultOptions,
        path: defaultOptions.path.replace('<symbols>', symbols)
    };

    let data = '';
    https.get(options, (resp) => {
        resp.on('data', (chunk) => {
            data += chunk;
        });
        resp.on('end', () => {
            const results = JSON.parse(data);
            const response = results.Message 
                ? { error: results.Message }
                : { data: results.data };
            
            return callback(response);
        });
        resp.on('error', (error) => {
            return callback({ error: error });
        });
    });
};