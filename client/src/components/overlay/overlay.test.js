import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { Overlay } from './overlay';

describe('Overlay', () => {
const overlay = () => shallow(<Overlay>This is a test</Overlay>);

  	it('should render', () => {
		const comp = overlay();
		
		expect(toJson(comp)).toMatchSnapshot();
	});
});