import React from 'react';
import { connect } from "react-redux";

import Spinner from '../spinner/spinner';
import { getErrorMessage, isLoading } from '../../redux/selectors';
import { clearErrors } from '../../redux/actions';

import './overlay.css';

export class Overlay extends React.Component {
	generateTextContent = () => {
		return (
			<div className="text">
				<div>
					<i className="fas fa-exclamation-triangle" />
					{this.props.error}
				</div>
				<button 
					type="button"
					className="btn btn-dark btn-sm"
					onClick={this.props.clearErrors}
				>Dismiss</button>
			</div>
		);
	};

	render () {
		const overlayContent = (this.props.error && this.generateTextContent()) 
            || (this.props.isLoading && <Spinner />) 
			|| null;
			
		return (
		    <div className="overlay" style={ { display: overlayContent ? 'block' : 'none' } }>
                <div className="content">
					{overlayContent}
				</div>
            </div>
	    );
  	}
}

const mapStateToProps = state => ({
    isLoading: isLoading(state),
    error: getErrorMessage(state)
});
  
const mapDispatchToProps = {
	clearErrors
};
  
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Overlay);