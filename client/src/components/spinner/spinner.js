import React from 'react';

import './spinner.css';

export class Spinner extends React.Component {
	render () {
		return (
            <div className="spinner fa-3x">
                <i className="fas fa-spinner fa-2x fa-pulse" />
            </div>    
	    );
    }			
}

export default Spinner;
