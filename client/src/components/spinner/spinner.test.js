import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { Spinner } from './spinner';

describe('Spinner', () => {
  	const spinner = () => shallow(<Spinner />);

  	it('should render', () => {
		const comp = spinner();
		
		expect(toJson(comp)).toMatchSnapshot();
	});
});