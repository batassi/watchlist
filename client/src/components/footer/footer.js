import React from 'react';
import { connect } from "react-redux";

import { hasSelectedRows } from '../../redux/selectors';
import { addQuote, refreshQuotes, toggleModal } from '../../redux/actions';

import './footer.css';

export class Footer extends React.Component {
	addQuote = () => {
		const symbol = this.refs.symbol.value;
		this.props.addQuote(symbol);
		this.refs.symbol.value = '';
	}

	render () {
		return (
		    <div className="add-form">
			    <span className="label">Add Symbol</span>
                <input className="form-control" type="text" ref="symbol"/>
                <button type="button" className="btn btn-primary btn-sm" onClick={this.addQuote}>
					Add
					<i className="fas fa-plus"></i>
				</button>

				<div className="action-toolbar float-right">
					<button 
						type="button" 
						className="btn btn-danger btn-sm"
						disabled={this.props.disableBtn}
						onClick={() => this.props.toggleModal(true)}
					>
						Remove
						<i className="far fa-trash-alt" />
					</button>
					<button 
						type="button" 
						className="btn btn-info btn-sm"
						disabled={this.props.disableBtn}
						onClick={this.props.refreshQuotes}
					>
						Refresh
						<i className="fas fa-sync" />
					</button>
				</div>
		    </div>
	    )
    } 			
}

const mapStateToProps = state => ({
	disableBtn: !hasSelectedRows(state)
});
  
const mapDispatchToProps = {
	addQuote,
	refreshQuotes,
	toggleModal
};
  
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Footer);
