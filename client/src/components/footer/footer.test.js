import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { Footer } from './footer';

describe('Footer', () => {
    const defaultProps = {

    };

  	const footer = (props) => shallow(<Footer {...defaultProps} {...props} />);

  	it('should render', () => {
		const comp = footer();
		
		expect(toJson(comp)).toMatchSnapshot();
    });
    
    it('should render with error', () => {
        const comp = footer({ error: 'test error' });

        expect(toJson(comp)).toMatchSnapshot();
    });

    it('should call addQuote', () => {
        const mockAddQuote = jest.fn();
        const mockSymbol = 'ABC';
        const comp = footer({ addQuote: mockAddQuote });

        comp.instance().refs = { symbol: { value: mockSymbol } }; 
        comp.instance().addQuote();

        expect(mockAddQuote).toHaveBeenCalledWith(mockSymbol);
    });
});