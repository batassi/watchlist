import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { QuoteRange } from './quoteRange';

describe('QuoteRange', () => {
    const defaultProps = {
        high: '10.999',
        low: '3.7899'
    };

  	const quote = (props) => shallow(<QuoteRange {...defaultProps} {...props} />);

  	it('should render', () => {
		const comp = quote();
		
		expect(toJson(comp)).toMatchSnapshot();
    });
});