import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import mockStock from '../../mock/stock.json';

import { Quote } from './quote';

describe('Quote', () => {
    const defaultProps = {
        stock: mockStock
    };

  	const quote = (props) => shallow(<Quote {...defaultProps} {...props} />);

  	it('should render', () => {
		const comp = quote();
		
		expect(toJson(comp)).toMatchSnapshot();
    });

    it('should format name', () => {
        const comp = quote();

        expect(comp.instance().getName('Test Stock', 'ABC')).toBe('Test Stock (ABC)');
    });


    it('should format ExChange Name', () => {
        const comp = quote();

        expect(comp.instance().getExchange('New York Stock Exchange', 'USD')).toBe('New York Stock Exchange - USD');
    });
});