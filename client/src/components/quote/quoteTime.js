import React from 'react';

export class QuoteTime extends React.Component {
  	render () {
        const dt = new Date(this.props.timestamp);

        const hours = dt.getHours() > 12 ? dt.getHours() - 12 : dt.getHours();
        const minutes = dt.getMinutes() < 10 ? '0' + dt.getMinutes() : dt.getMinutes();
        const amPm = dt.getHours() > 12 ? ' PM' : ' AM';
        const time = hours + ':' + minutes + amPm;

  		return (
    		<div className="quote-last-trade">
                <span className="label">Last Trade:</span>
                <br />
                <span className="date">{dt.toDateString()}</span>
                <span className="time">
                      {time}
                      {' (' + this.props.timezone + ')'}
                </span>
            </div>
		)
	}
}

// QuoteTime.prototype = {
//     timestamp: Proptypes.percent,
//     timezone: Proptypes.string
// };

export default QuoteTime;
