import React from 'react';

export class QuoteChange extends React.Component {
  	render () {
        const className = 'quote-change' + ( this.props.value > 0 ? ' increase' : ' decrease' );
  		return (
    		<div className={className}>
                {this.props.value > 0 
                    ? <i className="fas fa-caret-up" />
                    : <i className="fas fa-caret-down" /> 
                }
                {this.props.value}
                {this.props.percent ? '%' : ''}
            </div>
		)
	}
}

// QuoteChange.prototype = {
//     percent: Proptypes.percent,
//     value: Proptypes.string
// };

export default QuoteChange;
