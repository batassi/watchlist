import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { QuoteTime} from './quoteTime';

describe('QuoteTime', () => {
    const defaultProps = {
        timestamp: new Date('12/01/2019 4:00 PM').getTime(),
        timezone: 'EST'
    };

  	const quote = (props) => shallow(<QuoteTime {...defaultProps} {...props} />);

  	it('should render', () => {
		const comp = quote();
		
		expect(toJson(comp)).toMatchSnapshot();
    });
});