import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { QuoteChange } from './quoteChange';

describe('QuoteChange', () => {
    const defaultProps = {
        value: 1.34,
        percent: true
    };

  	const quote = (props) => shallow(<QuoteChange {...defaultProps} {...props} />);

  	it('should render - positive percent value', () => {
		const comp = quote();
		
		expect(toJson(comp)).toMatchSnapshot();
    });

    it('should render - positive value', () => {
		const comp = quote({ percent: false });
		
		expect(toJson(comp)).toMatchSnapshot();
    });

    it('should render - negative percent value', () => {
		const comp = quote({ value: -1.24, percent: true });
		
		expect(toJson(comp)).toMatchSnapshot();
    });

    it('should render - negative value', () => {
		const comp = quote({ value: -1.24, percent: false });
		
		expect(toJson(comp)).toMatchSnapshot();
    });
});