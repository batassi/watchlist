import React from 'react';

export class QuoteRange extends React.Component {
  	render () {
  		return (
    		<div className="quote-range-values">
                {parseFloat(this.props.low).toFixed(2)}
                <span className="separator">-</span> 
                {parseFloat(this.props.high).toFixed(2)}
            </div>
		)
	}
}

// QuoteRange.prototype = {
//     high: Proptypes.number,
//     low: Proptypes.number
// };

export default QuoteRange;
