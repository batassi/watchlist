import React from 'react';
import { connect } from "react-redux";

import QuoteChange from './quoteChange';
import QuoteRange from './quoteRange';
import QuoteTime from './quoteTime';

import { getSelectedQuote } from '../../redux/selectors';

import './quote.css';

export class Quote extends React.Component {
    getName (name, symbol) {
        return name + ' (' + symbol + ')';
    }

    getExchange (name, currency) {
        return name + ' - ' + currency;
    }

  	render () {
  		return (
    		<div className="quote-details">
                { this.props.stock 
                    ? <div className="row">
                        <div className="col-2">
                            <div className="quote-label">Quote Details</div>
                        </div>
                        <div className="col-3">
                            <div className="quote-name">{this.getName(this.props.stock.name, this.props.stock.symbol)}</div>
                            <div className="quote-exchange">{this.getExchange(this.props.stock.stock_exchange_long, this.props.stock.currency)}</div>
                        </div>
                        <div className="col-1">
                            <div className="quote-price">{this.props.stock.price}</div>
                        </div>
                        <div className="col-2">
                            <div className="quote-change-label">Day Change</div>
                            <QuoteChange value={this.props.stock.day_change} />
                            <QuoteChange value={this.props.stock.change_pct} percent={true} />
                        </div>
                        <div className="col-2">
                            <div className="quote-range-label">52 Week Range</div>
                            <QuoteRange low={this.props.stock['52_week_low']} high={this.props.stock['52_week_high']} />
                        </div>
                        <div className="col-2">
                            <QuoteTime timestamp={this.props.stock.last_trade_time} timezone={this.props.stock.timezone} />
                        </div>
                    </div>
                    : <div className="no-selection-msg">Please select a row below to see details</div>
                }
            </div>
		)
	}
}

// Quote.prototype = {
//     stock: Proptypes.object
// };

const mapStateToProps = state => ({
    stock: getSelectedQuote(state)
});
  
export default connect(
	mapStateToProps,
	{}
)(Quote);
