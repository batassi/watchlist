import React from 'react';

export class Header extends React.Component {
	render () {
		return (
		    <nav className="navbar navbar-dark bg-dark">
				<span className="navbar-brand mb-0 h1">Watchlist Dashboard</span>
		  	</nav>
	    );
  	}
}

export default Header;