import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { Header } from './header';

describe('Header', () => {
  	const header = () => shallow(<Header />);

  	it('should render', () => {
		const comp = header();
		
		expect(toJson(comp)).toMatchSnapshot();
	});
});