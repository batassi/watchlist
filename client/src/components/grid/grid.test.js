import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { Grid } from './grid';

describe('Grid', () => {
    const defaultProps = {};

  	const grid = (props) => shallow(<Grid {...defaultProps} {...props} />);

  	it('should render', () => {
		const comp = grid();
		
		expect(toJson(comp)).toMatchSnapshot();
    });

    it('should call deSelectAll when props are updated', () => {
        const comp = grid({ selectedRows: ['ABC', 'DEF'] });

        const mockDeselectAll = jest.fn();
        comp.instance().gridReady({
            api: {
                deselectAll: mockDeselectAll,
                sizeColumnsToFit: jest.fn(),
                hideOverlay: jest.fn(),
                getSelectedRows: () => [ { symbol: 'ABC' }, { symbol: 'DEF' }]
            }
        });

        comp.setProps({ selectedRows: [] });
        expect(mockDeselectAll).toHaveBeenCalled();
    });

    it('should not call deSelectAll when props are updated', () => {
        const comp = grid({ selectedRows: ['ABC', 'DEF'] });

        const mockDeselectAll = jest.fn();
        comp.instance().gridReady({
            api: {
                deselectAll: mockDeselectAll,
                sizeColumnsToFit: jest.fn(),
                hideOverlay: jest.fn(),
                getSelectedRows: () => [ { symbol: 'ABC' }, { symbol: 'DEF' }]
            }
        });

        comp.setProps({ selectedRows: ['ABC'] });
        expect(mockDeselectAll).not.toHaveBeenCalled();
    });
    
    it('should call setSelectedQuote when row is clicked', () => {
        const mockSetSelectedQuote = jest.fn();
        const comp = grid({ setSelectedQuote:  mockSetSelectedQuote });
        
        comp.instance().onSelectRow({ data: { symbol: 'ABC' }});
        expect(mockSetSelectedQuote).toHaveBeenCalledWith('ABC');
    });

    it('shoudl call setSelectedRows when rows are selected', () => {
        const mockSetSelectedRows = jest.fn();
        const comp = grid({ setSelectedRows:  mockSetSelectedRows });

        comp.instance().gridReady({
            api: {
                sizeColumnsToFit: jest.fn(),
                hideOverlay: jest.fn(),
                getSelectedRows: () => [ { symbol: 'ABC' }, { symbol: 'DEF' }]
            }
        });
        comp.instance().onSelectionChange();

        expect(mockSetSelectedRows).toHaveBeenCalledWith(['ABC', 'DEF']);
    });
});