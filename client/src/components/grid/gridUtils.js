/**
 * Utils to format data in the grid
 */

export const formatNumber = (number) => {
    const strArray = number.toString().split('.');

    let integer = strArray[0];
    const decimal = strArray.length > 1 ? '.' + strArray[1] : '';

    if (integer.length > 3) {
        //format number
        integer = integer.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    return integer + decimal;
};