import React from 'react';
import { connect } from "react-redux";
import { AgGridReact } from '@ag-grid-community/react';
import {AllCommunityModules} from '@ag-grid-community/all-modules';

import Overlay from '../overlay/overlay';

import { formatNumber } from './gridUtils';
import { getWatchList, getSelectedRows } from '../../redux/selectors';
import { setSelectedQuote, setSelectedRows } from '../../redux/actions';

import '@ag-grid-community/all-modules/dist/styles/ag-grid.css';
import '@ag-grid-community/all-modules/dist/styles/ag-theme-balham-dark.css';
import './grid.css';

export class Grid extends React.Component {
    columnDefs = [
		{
            headerName: "Symbol",
            checkboxSelection: true,
			field: "symbol"
		}, 
		{
			headerName: "Open",
            field: "price_open",
            valueFormatter: (params) => { return formatNumber(params.value); }
		},
		{
			headerName: "Close",
            field: "close_yesterday",
            valueFormatter: (params) => { return formatNumber(params.value); }
        },
        {
            headerName: "High",
            field: "day_high",
            valueFormatter: (params) => { return formatNumber(params.value); }
        },
        {
            headerName: "Low",
            field: "day_low",
            valueFormatter: (params) => { return formatNumber(params.value); }
        },
        {
            headerName: "Volume",
            field: "volume",
            valueFormatter: (params) => { return formatNumber(params.value); }
        }
    ];

    componentDidUpdate(prevProps) {
        if (this.props.selectedRows !== prevProps.setSelectedRows 
            && this.props.selectedRows.length === 0
            && this.gridApi ) {
            this.gridApi.deselectAll();
        }
    }

    autoSizeColumns = (params) => {
        const allCols = this.columnDefs.map(col => col.field);
        params.columnApi && params.columnApi.autoSizeColumns(allCols);
    };
    
    gridReady = (params) => {
        this.gridApi = params.api;
        this.gridApi.hideOverlay();
        
        this.autoSizeColumns(params);
    };

    onGridSizeChanged = (params) => {
        this.autoSizeColumns(params);
        this.gridApi.sizeColumnsToFit();
    };

    onSelectRow = (params) => {
        this.props.setSelectedQuote(params.data.symbol);
    };

    onSelectionChange = () => {
        const selectedRows = this.gridApi.getSelectedRows().map(row => row.symbol);
        this.props.setSelectedRows(selectedRows);
    };

  	render () {
  		return (
    		<div className="watchlist-grid ag-theme-balham-dark" style={ {height: '300px', width: '100%'} }> 
                <AgGridReact
                    columnDefs={this.columnDefs}
                    rowData={this.props.rowData}
                    onGridReady={this.gridReady}
                    onGridSizeChanged={this.onGridSizeChanged}
                    onRowClicked={this.onSelectRow}
                    onSelectionChanged={this.onSelectionChange}
                    rowSelection="multiple"
                    suppressRowClickSelection
                    modules={AllCommunityModules}
                />

                <Overlay />
            </div>
		)
	}
}

const mapStateToProps = state => ({
    rowData: getWatchList(state),
    selectedRows: getSelectedRows(state)
});
  
const mapDispatchToProps = {
    setSelectedQuote,
    setSelectedRows
};
  
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Grid);