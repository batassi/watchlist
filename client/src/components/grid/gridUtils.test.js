import * as Utils from './gridUtils';

describe('formatNumber', () => {
    it('should format integer properly', () => {
        expect(Utils.formatNumber(123124324)).toBe('123,124,324');
    });

    it('should format decimal properly', () => {
        expect(Utils.formatNumber(123124.13)).toBe('123,124.13');
    });

    it('should format small interger properly', () => {
        expect(Utils.formatNumber(123)).toBe('123');
    });

    it('should format small decimal properly', () => {
        expect(Utils.formatNumber(123.13)).toBe('123.13');
    });
});