import React from 'react';

import './deleteModal.css';

export class DeleteModal extends React.Component {
	render () {
		return (
            <div className="modal-open">
                <div className="modal fade show" tabIndex="-1" role="dialog" aria-modal="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Removing Quotes</h5>
                            </div>
                            <div className="modal-body">
                                <p>Your are about to remove the following quote(s) from your watchlist:</p>
                                <div className="quotes">{this.props.quotes.join(',')}</div>
                                <p>Do you want to proceed ?</p>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary" onClick={this.props.yesAction}>Yes</button>
                                <button type="button" className="btn btn-secondary" onClick={this.props.noAction}>No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    );
    }			
}

export default DeleteModal;
