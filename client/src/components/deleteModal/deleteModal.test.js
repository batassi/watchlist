import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { DeleteModal } from './deleteModal';

describe('DeleteModal', () => {
    const defaultProps = {
        quotes: ['ABC', 'DEF']
    };

  	const modal = (props) => shallow(<DeleteModal {...defaultProps} {...props} />);

  	it('should render', () => {
		const comp = modal();
		
		expect(toJson(comp)).toMatchSnapshot();
    });
    
    it('should call yesAction when Yes Button is clicked', () => {
        const mockAction = jest.fn();
        const comp = modal({ yesAction: mockAction });

        comp.find('button').at(0).simulate('click');

        expect(mockAction).toHaveBeenCalled();
    });

    it('should call noAction when No Button is clicked', () => {
        const mockAction = jest.fn();
        const comp = modal({ noAction: mockAction });

        comp.find('button').at(1).simulate('click');

        expect(mockAction).toHaveBeenCalled();
    });
});