import React from 'react';
import { connect } from "react-redux";

import Header from './components/header/header';
import Quote from './components/quote/quote';
import Grid from './components/grid/grid';
import Footer from './components/footer/footer';
import DeleteModal from './components/deleteModal/deleteModal';

import { fetchWatchListIfNeeded, setSelectedRows, removeQuotes, toggleModal } from './redux/actions';
import { showModal, getSelectedRows } from './redux/selectors';

import './App.css';

export class App extends React.Component {
	componentDidMount () {
		this.props.fetchWatchListIfNeeded();
	}

	closeModal = () => {
		this.props.toggleModal(false);
		this.props.setSelectedRows([]);
	};

	deleteQuotes = () => {
		this.props.removeQuotes();
		this.closeModal();
	}

  	render () {
  		return (
    		<div className="Watchlist-Dashboard">
      			<Header />
				
				<div className="container-fluid">
					<Quote />

					<div className="table-header">My Watchlist</div>

					<Grid />

					<Footer />
				</div>
				{this.props.showModal &&
					<DeleteModal 
						noAction={this.closeModal}
						quotes={this.props.selectedRows} 
						yesAction={this.deleteQuotes}
					/>
				}
    		</div>
		);
	}
}

const mapStateToProps = state => ({
	selectedRows: getSelectedRows(state),
	showModal: showModal(state)
});
  
const mapDispatchToProps = {
	fetchWatchListIfNeeded,
	removeQuotes,
	setSelectedRows,
	toggleModal
};
  
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App);