import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { App } from './App';

describe('App', () => {
  	const defaultProps = {
		fetchWatchListIfNeeded: jest.fn()
  	};

  	const app = (props) => shallow(<App {...defaultProps} {...props} />);

  	it('should render - without delete modal', () => {
		const comp = app();
		
		expect(toJson(comp)).toMatchSnapshot();
	});
	  
	it('should render - with delete modal', () => {
		const comp = app({ 
			selectedRows: ['ABC', 'DEF'],
			showModal: true 
		});

		expect(toJson(comp)).toMatchSnapshot();
	});

	it('should call fetchWatchListIfNeeded', () => {
		const mockFetchWatchListIfNeeded = jest.fn();
		const comp = app({ fetchWatchListIfNeeded: mockFetchWatchListIfNeeded });

		expect(mockFetchWatchListIfNeeded).toHaveBeenCalled();
	})
});