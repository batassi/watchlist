import * as types from './constants';
import * as status from './status';

export const initialState = {
    addQuote: {
        status: status.UNLOADED
    },
    removeQuote: {
        status: status.UNLOADED
    },
    selectedQuote: null,
    selectedRows: [],
    showModal: false,
    updateQuote: {
        status: status.UNLOADED
    },
    watchList: {
        quotes: {},
        status: status.UNLOADED
    }
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case types.SELECT_QUOTE:
            return {
                ...state,
                selectedQuote: action.quote
            };

        case types.SELECT_ROWS:
            return {
                ...state,
                selectedRows: action.quotes
            };

        case types.FETCH_WATCHLIST_REQUEST:
            return {
                ...state,
                watchList: {
                    ...state.watchList,
                    status: status.LOADING
                }
            };

        case types.FETCH_WATCHLIST_SUCCESS: {
                const quotes = {};
                action.data.forEach(quote => {
                    quotes[quote.symbol] = quote;
                });

                return {
                    ...state,
                    watchList: {
                        ...state.watchList,
                        quotes: quotes,
                        status: status.LOADED
                    }
                };
            }

        case types.FETCH_WATCHLIST_ERROR:
            return {
                ...state,
                watchList: {
                    ...state.watchList,
                    status: status.ERROR,
                    error: action.error
                }
            };

        case types.UPDATE_QUOTE_REQUEST:
            return {
                ...state,
                updateQuote: {
                    ...state.updateQuote,
                    status: status.LOADING
                }
            };

        case types.UPDATE_QUOTE_SUCCESS: {
                const tempWatchList = state.watchList.quotes;
                action.payload.forEach(quote => {
                    tempWatchList[quote.symbol] = quote;
                });

                return {
                    ...state,
                    addQuote: {
                        ...state.addQuote,
                        status: status.LOADED,
                        error: undefined
                    },
                    updateQuote: {
                        ...state.updateQuote,
                        status: status.LOADED,
                        error: undefined
                    },
                    watchList: {
                        ...state.watchList,
                        quotes: tempWatchList
                    }
                };
            }

        case types.UPDATE_QUOTE_ERROR:
            return {
                ...state,
                updateQuote: {
                    ...state.updateQuote,
                    status: status.ERROR,
                    error: action.error
                }
            };

        case types.ADD_QUOTE_REQUEST:
            return {
                ...state,
                addQuote: {
                    ...state.addQuote,
                    status: status.LOADING
                }
            };

        case types.ADD_QUOTE_ERROR:
            return {
                ...state,
                addQuote: {
                    ...state.addQuote,
                    status: status.ERROR,
                    error: action.error
                }
            };

        case types.REMOVE_QUOTES_REQUEST:
            return {
                ...state,
                removeQuote: {
                    ...state.removeQuote,
                    status: status.LOADING
                }
            };

        case types.REMOVE_QUOTES_SUCCESS: {
            const newState = state;
            action.quotes.forEach(quote => {
                delete newState.watchList.quotes[quote];
            });

            return {
                ...newState,
                removeQuote: {
                    status: status.LOADED
                }
            };
        }

        case types.REMOVE_QUOTES_ERROR:
            return {
                ...state,
                removeQuote: {
                    ...state.removeQuote,
                    status: status.ERROR,
                    error: action.error
                }
            };

        case types.SHOW_MODAL:
            return {
                ...state,
                showModal: action.flag
            };

        case types.CLEAR_ERRORS:
            return {
                ...state,
                addQuote: {
                    ...state.addQuote,
                    status: status.LOADED,
                    error: undefined
                },
                removeQuote: {
                    ...state.removeQuote,
                    status: status.LOADED,
                    error: undefined
                },
                updateQuote: {
                    ...state.updateQuote,
                    status: status.LOADED,
                    error: undefined
                },
                watchList: {
                    ...state.watchList,
                    status: status.LOADED,
                    error: undefined
                }
            };

        default: 
            return state;
    }
};

export default reducer;