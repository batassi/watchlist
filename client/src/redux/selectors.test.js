import * as status from './status';
import {
    getWatchList,
    getErrorMessage,
    getSelectedQuote,
    getSelectedRows,
    isLoading,
    hasSelectedRows,
    showModal
} from './selectors';

describe('selectors', () => {
    const initialState = (options) => ({
        addQuote: {
            status: status.UNLOADED
        },
        removeQuote: {
            status: status.UNLOADED
        },
        selectedQuote: null,
        selectedRows: [],
        showModal: false,
        updateQuote: {
            status: status.UNLOADED
        },
        watchList: {
            quotes: {},
            status: status.UNLOADED
        },
        ...options
    });

    it('should return watchlist', () => {
        const watchList = {
            quotes: {
                'abc': { price: '124.45'},
                'def': { price: '456.33 '}
            }
        };
        const state = initialState({ watchList });
        expect(getWatchList(state)).toEqual(Object.values(watchList.quotes));
    });

    describe('should return error message', () => {
        it('watchlist', () => {
            const state = initialState({ watchList: { error: 'mock error'}});
            expect(getErrorMessage(state)).toEqual('mock error');
        });

        it('addQuote', () => {
            const state = initialState({ addQuote: { error: 'mock error'}});
            expect(getErrorMessage(state)).toEqual('mock error');
        });

        it('removeQuote', () => {
            const state = initialState({ removeQuote: { error: 'mock error'}});
            expect(getErrorMessage(state)).toEqual('mock error');
        });

        it('updateQuote', () => {
            const state = initialState({ updateQuote: { error: 'mock error'}});
            expect(getErrorMessage(state)).toEqual('mock error');
        });
    });

    describe('should return data for selected quote', () => {
        const watchList = {
            quotes: {
                'abc': { price: '124.45'},
                'def': { price: '456.33 '}
            }
        };

        it('defined selected quote', () => {
            const state = initialState({ watchList, selectedQuote: 'abc' });
            expect(getSelectedQuote(state)).toEqual(watchList.quotes['abc']);
        });

        it('undefined selected quotes', () => {
            const state = initialState({ watchList });
            expect(getSelectedQuote(state)).toBeFalsy();
        });
    });

    it('should return selected rows', () => {
        const state = initialState({ selectedRows: ['abc', 'def'] });
        expect(getSelectedRows(state)).toEqual(['abc', 'def']);
    });

    describe('should return loading status', () => {
        it('unloaded', () => {
            const state = initialState();
            expect(isLoading(state)).toBeFalsy();
        });

        it('loading - watchlist', () => {
            const watchList = {
                quotes: {},
                status: status.LOADING
            };
            const state = initialState({ watchList });
            expect(isLoading(state)).toBeTruthy();
        });

        it('loading - addQuote', () => {
            const addQuote = {
                status: status.LOADING
            };
            const state = initialState({ addQuote });
            expect(isLoading(state)).toBeTruthy();
        });

        it('loading - removeQuote', () => {
            const removeQuote = {
                status: status.LOADING
            };
            const state = initialState({ removeQuote });
            expect(isLoading(state)).toBeTruthy();
        });

        it('loading - updateQuote', () => {
            const updateQuote = {
                status: status.LOADING
            };
            const state = initialState({ updateQuote });
            expect(isLoading(state)).toBeTruthy();
        });

        it('loaded', () => {
            const watchList = {
                quotes: {},
                status: status.LOADED
            };
            const state = initialState({ watchList });
            expect(isLoading(state)).toBeFalsy();
        });
    });

    describe('should return wether there are selected rows or not', () => {
        it('has selected rows', () => {
            const state = initialState({ selectedRows: ['abc', 'def'] });
            expect(hasSelectedRows(state)).toBeTruthy();
        });

        it('has no selected rows', () => {
            const state = initialState({ selectedRows: [] });
            expect(hasSelectedRows(state)).toBeFalsy();
        });
    });

    describe('should return show modal flag', () => {
        it('flag set to false', () => {
            const state = initialState();
            expect(showModal(state)).toBeFalsy();
        });

        it('flag set to true', () => {
            const state = initialState({ showModal: true });
            expect(showModal(state)).toBeTruthy();
        });
    });
});