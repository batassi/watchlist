import * as status from './status';

/**
 * Return watchlist
 */
export const getWatchList = (state) => {
    return Object.values(state.watchList.quotes);
};

/**
 * Get Error Message
 */
export const getErrorMessage = (state) => {
    return state.watchList.error
        || state.addQuote.error
        || state.removeQuote.error
        || state.updateQuote.error;
};

/**
 * Return selected quote details
 */
export const getSelectedQuote = (state) => {
    return state.selectedQuote ? state.watchList.quotes[state.selectedQuote] : null;
};

/**
 * Get Select Rows
 */
export const getSelectedRows = (state) => {
    return state.selectedRows;
};

/**
 * Checks wether data is loading or not
 */
export const isLoading = (state) => {
    return state.watchList.status === status.LOADING 
    || state.addQuote.status === status.LOADING 
    || state.removeQuote.status === status.LOADING 
    || state.updateQuote.status === status.LOADING;
};

/**
 * Check wether there are some rows that are selcted
 */
export const hasSelectedRows = (state) => {
    return state.selectedRows.length > 0;
}

/**
 * Show Delete Modal
 */
export const showModal = (state) => {
    return state.showModal;
};