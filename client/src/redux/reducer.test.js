import * as types from './constants';
import * as status from './status';
import reducer, { initialState } from './reducer';

describe('Reducer', () => {
    it('should return initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle SELECT_QUOTE', () => {
        const updatedState = {
            selectedQuote: 'ABC'
        };

        const action = {
            type: types.SELECT_QUOTE,
            quote: 'ABC'
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle SELECT_ROWS', () => {
        const updatedState = {
            selectedRows: ['ABC', 'DEF']
        };

        const action = {
            type: types.SELECT_ROWS,
            quotes: ['ABC', 'DEF']
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle FETCH_WATCHLIST_REQUEST', () => {
        const updatedState = {
            watchList: {
                status: status.LOADING
            }
        };

        const action = {
            type: types.FETCH_WATCHLIST_REQUEST
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle FETCH_WATCHLIST_SUCCESS', () => {
        const updatedState = {
            watchList: {
                quotes : {
                    'ABC': { symbol: 'ABC', price: 10.99 },
                    'DEF': { symbol: 'DEF', price: 34.55 }
                },
                status: status.LOADED
            }
        };

        const action = {
            type: types.FETCH_WATCHLIST_SUCCESS,
            data: [
                { symbol: 'ABC', price: 10.99 },
                { symbol: 'DEF', price: 34.55 }
            ]
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle FETCH_WATCHLIST_ERROR', () => {
        const updatedState = {
            watchList: {
                status: status.ERROR,
                error: 'mock error message'
            }
        };

        const action = {
            type: types.FETCH_WATCHLIST_ERROR,
            error: 'mock error message'
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });


    it('should handle UPDATE_QUOTE_REQUEST', () => {
        const updatedState = {
            updateQuote: {
                status: status.LOADING
            }
        };

        const action = {
            type: types.UPDATE_QUOTE_REQUEST
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle UPDATE_QUOTE_SUCCESS', () => {
        const quotes = {
            'ABC': { symbol: 'ABC', price: 11.88 },
            'DEF': { symbol: 'DEF', price: 34.55 }
        };
        const state = {
            watchList: {
                quotes
            }
        };

        const updatedState = {
            addQuote: {
                status: status.LOADED
            },
            updateQuote: {
                status: status.LOADED
            },
            watchList: {
                quotes: {
                    'ABC': { symbol: 'ABC', price: 12.99 },
                    'DEF': { symbol: 'DEF', price: 34.55 }
                }
            }
        };

        const action = {
            type: types.UPDATE_QUOTE_SUCCESS,
            payload: [
                { symbol: 'ABC', price: 12.99 }
            ]
        };

        expect(reducer(state, action)).toMatchObject(updatedState);
    });

    it('should handle UPDATE_QUOTE_ERROR', () => {
        const updatedState = {
            updateQuote: {
                status: status.ERROR,
                error: 'mock error message' 
            }
        };

        const action = {
            type: types.UPDATE_QUOTE_ERROR,
            error: 'mock error message'
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle ADD_QUOTE_REQUEST', () => {
        const updatedState = {
            addQuote: {
                status: status.LOADING
            }
        };

        const action = {
            type: types.ADD_QUOTE_REQUEST
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle ADD_QUOTE_ERROR', () => {
        const updatedState = {
            addQuote: {
                status: status.ERROR,
                error: 'mock error message' 
            }
        };

        const action = {
            type: types.ADD_QUOTE_ERROR,
            error: 'mock error message'
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle REMOVE_QUOTES_REQUEST', () => {
        const updatedState = {
            removeQuote: {
                status: status.LOADING
            }
        };

        const action = {
            type: types.REMOVE_QUOTES_REQUEST
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle REMOVE_QUOTES_SUCCESS', () => {
        const state = {
            watchList: {
                quotes: {
                    'ABC': { symbol: 'ABC', price: 11.88 },
                    'DEF': { symbol: 'DEF', price: 34.55 }
                }
            }
        };

        const updatedState = {
            watchList: {
                quotes: {
                    'DEF': { symbol: 'DEF', price: 34.55 }
                }
            },
            removeQuote: {
                status: status.LOADED
            }
        };

        const action = {
            type: types.REMOVE_QUOTES_SUCCESS,
            quotes: ['ABC']
        };

        expect(reducer(state, action)).toMatchObject(updatedState);
    });

    it('should handle REMOVE_QUOTES_ERROR', () => {
        const updatedState = {
            removeQuote: {
                status: status.ERROR,
                error: 'mock error message' 
            }
        };

        const action = {
            type: types.REMOVE_QUOTES_ERROR,
            error: 'mock error message'
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle SHOW_MODAL', () => {
        const updatedState = {
            showModal: true
        };

        const action = {
            type: types.SHOW_MODAL,
            flag: true
        };

        expect(reducer({}, action)).toMatchObject(updatedState);
    });

    it('should handle CLEAR ERRORS', () => {
        const state = {
            addQuote: {
                status: status.ERROR,
                error: 'mock error 1'
            },
            removeQuote: {
                status: status.ERROR,
                error: 'mock error 2'
            },
            updateQuote: {
                status: status.ERROR,
                error: 'mock error 3'
            },
            watchList: {
                status: status.ERROR,
                error: 'mock error 4'
            }
        };

        const updatedState = {
            addQuote: {
                status: status.LOADED
            },
            removeQuote: {
                status: status.LOADED
            },
            updateQuote: {
                status: status.LOADED
            },
            watchList: {
                status: status.LOADED
            }
        };

        const action = {
            type: types.CLEAR_ERRORS
        };

        expect(reducer(state, action)).toMatchObject(updatedState);
    });
});