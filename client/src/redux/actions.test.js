import configureStore from 'redux-mock-store';
import thunkMiddleware from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import mockWatchlist from '../mock/watchlist.json';

import * as types from './constants';
import * as status from './status';
import {
    fetchWatchListIfNeeded,
    setSelectedQuote,
    setSelectedRows,
    addQuote,
    removeQuotes,
    refreshQuotes,
    toggleModal,
    clearErrors
} from './actions';

describe('actions', () => {
    const initialState = (options) => ({
        addQuote: {
            status: status.UNLOADED
        },
        removeQuote: {
            status: status.UNLOADED
        },
        selectedQuote: null,
        selectedRows: [],
        showModal: false,
        updateQuote: {
            status: status.UNLOADED
        },
        watchList: {
            quotes: {},
            status: status.UNLOADED
        },
        ...options
    });

    
    const mockStore = (options) => configureStore([thunkMiddleware])(initialState(options));
    const mockAxios = new MockAdapter(axios);

    describe('fetchWatchListIfNeeded', () => {
        it('should dispatch the correct actions - success', () => {
            mockAxios.onGet('http://localhost:5000/api/watchlist')
                .reply(200, mockWatchlist);
            
            const store = mockStore();
            return store.dispatch(fetchWatchListIfNeeded())
                .then(() => {
                    expect(store.getActions()).toEqual([
                        { type: types.FETCH_WATCHLIST_REQUEST },
                        { type: types.FETCH_WATCHLIST_SUCCESS, data: mockWatchlist.data }
                    ]);
                });
        });

        it('should dispatch the correct actions - error', () => {
            mockAxios.onGet('http://localhost:5000/api/watchlist')
                .reply(500, { error: 'mock error message' });
            
            const store = mockStore();
            return store.dispatch(fetchWatchListIfNeeded())
                .then(() => {
                    expect(store.getActions()).toEqual([
                        { type: types.FETCH_WATCHLIST_REQUEST },
                        { type: types.FETCH_WATCHLIST_ERROR, error: 'mock error message' }
                    ]);
                });
        });
    });

    describe('setSelectedQuote', () => {
        it('should dispatch correct action', () => {
            const store = mockStore();
            store.dispatch(setSelectedQuote('ABC'));
            expect(store.getActions()).toEqual([
                { type: types.SELECT_QUOTE, quote: 'ABC' }
            ]);
        });
    });

    describe('setSelectedRows', () => {
        it('should dispatch the correct action', () => {
            const store = mockStore();
            store.dispatch(setSelectedRows(['abc', 'def']));
            expect(store.getActions()).toEqual([
                { type: types.SELECT_ROWS, quotes: ['abc', 'def'] }
            ]);
        });
    });

    describe('addQuote', () => {
        it('should dispatch correct actions - existing quote', () => {
            const watchList = {
                quotes: {
                    'SPY': mockWatchlist.data[0],
                    'TSLA': mockWatchlist.data[3]
                }
            };
            const store = mockStore({ watchList });
            store.dispatch(addQuote('TSLA'));
            expect(store.getActions()).toEqual([
                { type: types.ADD_QUOTE_REQUEST },
                { type: types.ADD_QUOTE_ERROR, error: 'Quote already added to watchlist' }
            ]);
        });

        it('should dispatch correct actions - new quote - success', () => {
            const mockStock = { data: [].concat(mockWatchlist.data[0]) };
            mockAxios.onPost('http://localhost:5000/api/watchlist/insert')
                .reply(200, mockStock);

            const watchList = {
                quotes: {
                    'SPY': mockWatchlist.data[0],
                    'TSLA': mockWatchlist.data[3]
                }
            };
            const store = mockStore({ watchList });
            return store.dispatch(addQuote('MSFT'))
                .then(() => {
                    expect(store.getActions()).toEqual([
                        { type: types.ADD_QUOTE_REQUEST },
                        { type: types.UPDATE_QUOTE_SUCCESS, payload: mockStock.data }
                    ]);
                });
            
        });

        it('should dispatch correct actions - new quote - error', () => {
            mockAxios.onPost('http://localhost:5000/api/watchlist/insert')
                .reply(500, { error: 'mock error message'});

            const watchList = {
                quotes: {
                    'SPY': mockWatchlist.data[0],
                    'TSLA': mockWatchlist.data[3]
                }
            };
            const store = mockStore({ watchList });
            return store.dispatch(addQuote('MSFT'))
                .then(() => {
                    expect(store.getActions()).toEqual([
                        { type: types.ADD_QUOTE_REQUEST },
                        { type: types.ADD_QUOTE_ERROR, error: 'mock error message' }
                    ]);
                });
            
        });
    });

    describe('removeQuotes', () => {
        it('should dispatch correct actions - success', () => {
            mockAxios.onDelete('http://localhost:5000/api/watchlist/remove')
                .reply(200, 'OK');

            const watchList = {
                quotes: {
                    'SPY': mockWatchlist.data[0],
                    'TSLA': mockWatchlist.data[3]
                }
            };
            const store = mockStore({ watchList, selectedRows: ['SPY'] });
            return store.dispatch(removeQuotes())
                .then(() => {
                    expect(store.getActions()).toEqual([
                        { type: types.REMOVE_QUOTES_REQUEST },
                        { type: types.REMOVE_QUOTES_SUCCESS, quotes: ['SPY'] },
                        { type: types.SELECT_ROWS, quotes: [] }
                    ]);
                });
            
        });

        it('should dispatch correct actions - error', () => {
            mockAxios.onDelete('http://localhost:5000/api/watchlist/remove')
                .reply(500, { error: 'mock error message'});

            const watchList = {
                quotes: {
                    'SPY': mockWatchlist.data[0],
                    'TSLA': mockWatchlist.data[3]
                }
            };
            const store = mockStore({ watchList, selectedRows: ['SPY'] });
            return store.dispatch(removeQuotes())
                .then(() => {
                    expect(store.getActions()).toEqual([
                        { type: types.REMOVE_QUOTES_REQUEST },
                        { type: types.REMOVE_QUOTES_ERROR, error: 'mock error message' },
                        { type: types.SELECT_ROWS, quotes: [] }
                    ]);
                });
            
        });
    });

    describe('refreshQuotes', () => {
        it('should dispatch correct actions - success', () => {
            const mockStock = { data: [].concat(mockWatchlist.data[0]) };
            mockAxios.onGet('http://localhost:5000/api/stock?symbols=SPY')
                .reply(200, mockStock);

            const watchList = {
                quotes: {
                    'SPY': mockWatchlist.data[0],
                    'TSLA': mockWatchlist.data[3]
                }
            };
            const store = mockStore({ watchList, selectedRows: ['SPY'] });
            return(store.dispatch(refreshQuotes()))
                .then(() => {
                    expect(store.getActions()).toEqual([
                        { type: types.UPDATE_QUOTE_REQUEST },
                        { type: types.UPDATE_QUOTE_SUCCESS, payload: mockStock.data },
                        { type: types.SELECT_ROWS, quotes: [] }
                    ]);
                });
        });

        it('should dispatch correct actions - success', () => {
            mockAxios.onGet('http://localhost:5000/api/stock?symbols=SPY')
                .reply(500, { error: 'mock error message' });

            const watchList = {
                quotes: {
                    'SPY': mockWatchlist.data[0],
                    'TSLA': mockWatchlist.data[3]
                }
            };
            const store = mockStore({ watchList, selectedRows: ['SPY'] });
            return(store.dispatch(refreshQuotes()))
                .then(() => {
                    expect(store.getActions()).toEqual([
                        { type: types.UPDATE_QUOTE_REQUEST },
                        { type: types.UPDATE_QUOTE_ERROR, error: 'mock error message' },
                        { type: types.SELECT_ROWS, quotes: [] }
                    ]);
                });
        });
    });

    describe('toggleModal', () => {
        it('should dispatch correct action - true', () => {
            const store = mockStore();
            store.dispatch(toggleModal(true));
            expect(store.getActions()).toEqual([
                { type: types.SHOW_MODAL, flag: true }
            ]);
        });

        it('should dispatch correct action - false', () => {
            const store = mockStore();
            store.dispatch(toggleModal(false));
            expect(store.getActions()).toEqual([
                { type: types.SHOW_MODAL, flag: false }
            ]);
        });
    });

    describe('clearErrors', () => {
        it('should dispatch the correct action', () => {
            const store = mockStore();
            store.dispatch(clearErrors());
            expect(store.getActions()).toEqual([
                { type: types.CLEAR_ERRORS }
            ]);
        });
    });
});