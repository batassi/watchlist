import axios from 'axios';

import * as types from './constants';
import * as status from './status';

const API_URL = 'http://localhost:5000/api';
const UPDATE_STOCK = '/stock';
const WATCH_LIST = '/watchlist';
const WATCH_LIST_ADD = '/watchlist/insert';
const WATCH_LIST_DEL = '/watchlist/remove';

const extractErrorMsg = (error) => {
    return error.response 
        ? error.response.data.error
        : error.toString();
};

/**
 * Get Watch List
 */
export const fetchWatchListIfNeeded = () => { 
    return (dispatch, getState) => {
        const state = getState();

        if (state.watchList.status === status.UNLOADED) {
            dispatch ({
                type: types.FETCH_WATCHLIST_REQUEST
            });

            return axios({
                method: 'GET',
                url: API_URL + WATCH_LIST,
            })
            .then(response => {
                dispatch ({
                    type: types.FETCH_WATCHLIST_SUCCESS,
                    data: response.data.data
                });
            })
            .catch(error => {
                dispatch ({
                    type: types.FETCH_WATCHLIST_ERROR,
                    error: extractErrorMsg(error)
                });
            });
        }
    };
};

/**
 * Set Selected Quote
 */
export const setSelectedQuote = (quote) => {
    return (dispatch) => {
        dispatch ({
            type: types.SELECT_QUOTE,
            quote
        });
    }
};

/**
 * Set Selected Rows in table
 */
export const setSelectedRows = (quotes) => {
    return (dispatch) => {
        dispatch({
            type: types.SELECT_ROWS,
            quotes
        });
    }
}

/**
 * Add Quote to WatchList
 */
export const addQuote = (symbol) => {
    return (dispatch, getState) => {
        dispatch({ type: types.ADD_QUOTE_REQUEST });
        const state = getState();
        
        if(state.watchList.quotes[symbol]) {
            //quote already in watchlist
            dispatch({
                type: types.ADD_QUOTE_ERROR,
                error: 'Quote already added to watchlist'
            });
        } else {
            //add quote in watchlist
            return axios({
                method: 'POST',
                url: API_URL + WATCH_LIST_ADD,
                data: { quote: symbol }
            })
            .then(response => {
                dispatch({
                    type: types.UPDATE_QUOTE_SUCCESS,
                    payload: response.data.data
                });
            })
            .catch(error => {
                dispatch ({
                    type: types.ADD_QUOTE_ERROR,
                    error: extractErrorMsg(error)
                });
            });
        }
    };
};

/**
 * Remove Quote from Watchlist
 */
export const removeQuotes = () => {
    return (dispatch, getState) => {
        dispatch({ type: types.REMOVE_QUOTES_REQUEST });
        const quotes = getState().selectedRows;

        return axios({
            method: 'DELETE',
            url: API_URL + WATCH_LIST_DEL,
            data: { quotes }
        })
        .then(response => {
            dispatch({
                type: types.REMOVE_QUOTES_SUCCESS,
                quotes
            });
            dispatch({
                type: types.SELECT_ROWS,
                quotes: []
            });
        })
        .catch( error => {
            dispatch ({
                type: types.REMOVE_QUOTES_ERROR,
                error: extractErrorMsg(error)
            });
            dispatch({
                type: types.SELECT_ROWS,
                quotes: []
            });
        });
    }
}

/**
 * Refresh Quote
 */
export const refreshQuotes = () => {
    return (dispatch, getState) => {
        dispatch({ type: types.UPDATE_QUOTE_REQUEST });
        const quotes = getState().selectedRows;

        return axios({
            method: 'GET',
            url: API_URL + UPDATE_STOCK + '?symbols=' + quotes.join(',')
        })
        .then(response => {
            dispatch({
                type: types.UPDATE_QUOTE_SUCCESS,
                payload: response.data.data
            });
            dispatch({
                type: types.SELECT_ROWS,
                quotes: []
            });
        })
        .catch( error => {
            dispatch ({
                type: types.UPDATE_QUOTE_ERROR,
                error: extractErrorMsg(error)
            });
            dispatch({
                type: types.SELECT_ROWS,
                quotes: []
            });
        });
    };
};

/**
 * Toggle Delete Modal
 */
export const toggleModal = (flag) => {
    return (dispatch) => {
        dispatch({
            type: types.SHOW_MODAL,
            flag
        });
    };
};

/**
 * Clear Errors
 */
export const clearErrors = () => {
    return (dispatch) => {
        dispatch({
            type: types.CLEAR_ERRORS
        });
    };
};