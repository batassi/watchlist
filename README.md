# watchlist Dashboard

## Run the API

```diff
cd api
npm install
npm start
```
(see [http://localhost:5000/api/](http://locahost:5000/api/) for list of available end points)

## Run the client 

```diff
cd client
npm install
npm start
```

## Load the application
[http://localhost:3000](http://localhost:3000);